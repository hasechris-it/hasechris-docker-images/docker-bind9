FROM alpine:latest

# old maintainer (forked from)
#MAINTAINER resyst-it <florian.cauzardjarry@gmail.com>

MAINTAINER hasechris <hase.christian92@gmail.com>

RUN apk --update --no-cache add bind bind-dnssec-tools \
    && chown 100:101 -R /etc/bind

EXPOSE 53

CMD ["named", "-c", "/etc/bind/named.conf", "-g", "-u", "named"]
